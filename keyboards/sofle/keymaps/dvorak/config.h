#pragma once

/* The way how "handedness" is decided (which half is which),
see https://docs.qmk.fm/#/feature_split_keyboard?id=setting-handedness
for more options.
*/

#undef OLED_TIMEOUT
#define OLED_TIMEOUT 45000

#undef TAPPING_TERM
#define TAPPING_TERM 250
#define IGNORE_MOD_TAP_INTERRUPT
/*#define PERMISSIVE_HOLD*/
