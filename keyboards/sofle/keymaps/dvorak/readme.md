![SofleKeyboard default keymap](https://github.com/josefadamcik/SofleKeyboard/raw/master/Images/soflekeyboard.png)
![SofleKeyboard adjust layer](https://github.com/josefadamcik/SofleKeyboard/raw/master/Images/soflekeyboard_layout_adjust.png)


# Milind Kamble's DVORAK keyboard layout. Presently not supporting
  qwerty layer.

Features:

- Symmetric modifiers (GUI/Meta, Alt, Ctrl, Shift). Mod-tap multiplexing of home row keys
- Arrow keys mapped on bottom row outer most two keys
- Only one layer needed to map arrow keys on right home row keys
- Left encoder controls volume up/down/mute. Right encoder PGUP/PGDOWN.
